package dresden23.model;

import com.sun.istack.internal.NotNull;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.*;
import javax.transaction.UserTransaction;
import java.util.List;

@Entity
@Table(name = "pickuppoints")
@RequestScoped
@ManagedBean( name="pickuppoint")

public class PickUpPoint {
    @NotNull @Id private int pointId;
    @NotNull private String name;
    @NotNull private String address;
    // default constructor
    public PickUpPoint() {}
    // constructor
    public PickUpPoint(int pointId, String name, String address) {
        this.setPointId(pointId);
        this.setName(name);
        this.setAddress(address);
    }
    public int getPointId() {
        return pointId;
    }
    public void setPointId(int pointId) {
        this.pointId = pointId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    // CRUD data management methods
    public static void create(EntityManager em, UserTransaction ut,
                              int pointId, String name, String address)
            throws Exception {
        ut.begin();
        PickUpPoint pickuppoint = new PickUpPoint( pointId, name, address);
        em.persist( pickuppoint);
        ut.commit();
    }

    public static List<PickUpPoint> retrieveAll(EntityManager em) {
        Query query = em.createQuery( "SELECT c FROM PickUpPoint c", PickUpPoint.class);
        List<PickUpPoint> pickuppoints = query.getResultList();
        return pickuppoints;
    }

    public static PickUpPoint retrieve(EntityManager em, int pointId) {
        PickUpPoint pickuppoint = em.find( PickUpPoint.class, pointId);
        if ( pickuppoint != null) {
            System.out.println( "PickUpPoint.retrieve: loaded pickuppoint " + pickuppoint);
        }
        return pickuppoint;
    }
    public static void update( EntityManager em,
                               UserTransaction ut, int pointId, String name, String address) throws Exception {
        ut.begin();
        PickUpPoint pickuppoint = em.find( PickUpPoint.class, pointId);
        if (!name.equals( pickuppoint.getName())) pickuppoint.setName( name);
        if (!address.equals( pickuppoint.getAddress())) pickuppoint.setAddress( address);
        ut.commit();
    }
    public static void delete( EntityManager em,
                               UserTransaction ut, int pointId) throws Exception {
        ut.begin();
        PickUpPoint pickuppoint = em.find( PickUpPoint.class, pointId);
        em.remove( pickuppoint);
        ut.commit();
    }
    public static void clearData( EntityManager em,
                                  UserTransaction ut) throws Exception {
        ut.begin();
        Query deleteStatement = em.createQuery( "DELETE FROM PickUpPoint");
        deleteStatement.executeUpdate();
        ut.commit();
    }
    public static void generateTestData( EntityManager em,
                                         UserTransaction ut) throws Exception {
        PickUpPoint pickuppoint = null;
        PickUpPoint.clearData( em, ut);  // first clear the books table
        ut.begin();
        pickuppoint = new PickUpPoint(1,"Airport", "Dummyaddress");
        em.persist( pickuppoint);
        pickuppoint = new PickUpPoint(2,"City Center", "Dummyaddress");
        em.persist( pickuppoint);
        pickuppoint = new PickUpPoint(3,"City South", "Dummyaddress");
        em.persist( pickuppoint);
        ut.commit();
    }
}
