package dresden23.model;

import dresden23.model.exception.IntervalConstraintViolation;
import dresden23.model.exception.MandatoryValueConstraintViolation;
import dresden23.model.exception.UniquenessConstraintViolation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.*;
import javax.persistence.RollbackException;
import javax.transaction.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table( name="cartypes")
@RequestScoped
@ManagedBean( name="cartype") @ViewScoped
public class CarType {
    @NotNull
    @Id
    private Integer cartypeId;
    @NotNull
    @Column( nullable=false)
    private String name;
    @ManyToMany( fetch=FetchType.EAGER, mappedBy="cartypes")
    private Set<Car> cartypedCars;
    //default constructor
    public CarType() {}
    // constructor
    public CarType (Integer cartypeId, String name, Set<Car> cartypedCars) {
        this.setCartypeId (cartypeId);
        this.setName (name);
        this.setCartypedCars(cartypedCars);
    }
    // getter + setter

    public Integer getCartypeId() {
        return cartypeId;
    }
    public void setCartypeId(Integer cartypeId) {
        this.cartypeId = cartypeId;
    }
    public String getName () {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Set<Car> getCartypedCars() {
        return cartypedCars;
    }

    public void setCartypedCars( Set<Car> cartypedCars) {
        Set<Car> oldCartypedCars = this.cartypedCars != null ? new HashSet<Car>(
                this.cartypedCars) : null;
        Set<Car> newCartypedCars = cartypedCars != null ? new HashSet<Car>(
                cartypedCars) : null;
        if ( oldCartypedCars != null) {
            for ( Car c : oldCartypedCars) {
                this.removeCartypedCar( c);
            }
        }
        // add this book to the authoredBooks of the new authors of this book
        if ( newCartypedCars != null) {
            for ( Car c : newCartypedCars) {
                this.addCartypedCar( c);
            }
        } else {
            if ( this.cartypedCars != null) {
                this.cartypedCars.clear();
            }
        }
    }

    public void addCartypedCar( Car cartypedCar) {
        if ( this.cartypedCars == null) {
            this.cartypedCars = new HashSet<Car>();
        }
        if ( !this.cartypedCars.contains( cartypedCar)) {
            this.cartypedCars.add( cartypedCar);
            cartypedCar.addCarType( this);
        }
    }

    public void removeCartypedCar( Car cartypedCar) {
        if ( this.cartypedCars != null && cartypedCar != null
                && this.cartypedCars.contains( cartypedCar)) {
            this.cartypedCars.remove( cartypedCar);
            cartypedCar.removeCarType( this);
        }
    }

    /**
     * Create a human readable serialization.
     */
    public String toString() {
        String result = "{ carTypeId: " + this.cartypeId + ", name:'" + this.name;
        result += ", carTypedCars: [";
        if ( this.cartypedCars != null) {
            int i = 0, n = this.cartypedCars.size();
            for ( Car c : this.cartypedCars) {
                result += "'" + c.getName() + "'";
                if ( i < n - 1) {
                    result += ", ";
                }
                i++;
            }
        }
        result += "]}";
        return result;
    }

    @Override
    public boolean equals( Object obj) {
        if (obj instanceof CarType) {
            CarType carType = (CarType) obj;
            return ( this.cartypeId.equals( carType.cartypeId));
        } else return false;
    }


    //check functions

    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws UniquenessConstraintViolation
     */
    public static void checkCarTypeIdAsId( EntityManager em, Integer cartypeId)
            throws UniquenessConstraintViolation,
            IntervalConstraintViolation {
        if (cartypeId < 1) {
            throw new IntervalConstraintViolation(
                    "CarTypeId must be at least 1");
        } else {
            CarType cartype = CarType.retrieve( em, cartypeId);
            // car was found, uniqueness constraint validation failed
            if ( cartype != null) {
                throw new UniquenessConstraintViolation(
                        "There is already a cartype record with this cartypeId!");
            }
        }
    }

    public static void checkName( EntityManager em, String name)
            throws MandatoryValueConstraintViolation {
        if (name == null) {
            throw new MandatoryValueConstraintViolation(
                    "Car Name is mandatory");
        }
    }


    // static methods
    /**
     * Create an Author instance.
     *
     * @param em
     *          reference to the entity manager
     * @param ut
     *          reference to the user transaction
     * @param cartypeId
     *          the cartypeId value of the cartype to create
     * @param name
     *          the name value of the cartype to create
     * @param cartypedCars
     *          the cartypedCars value of the cartype to create
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public static void create( EntityManager em, UserTransaction ut,
                            Integer cartypeId, String name, Set<Car> cartypedCars)
            throws NotSupportedException, SystemException, IllegalStateException,
            SecurityException, HeuristicMixedException,
            HeuristicRollbackException, RollbackException, EntityExistsException, javax.transaction.RollbackException {
        ut.begin();
        CarType carType = new CarType( cartypeId, name, cartypedCars);
        em.persist( carType);
        for ( Car c : carType.getCartypedCars()) {
            em.merge( c);
        }
        ut.commit();
        System.out.println( "Cartype.add: the Cartype " + carType + " was saved.");
    }

    public static List<CarType> retrieveAll(EntityManager em) {
        Query query = em.createQuery( "SELECT ct FROM CarType ct", CarType.class);
        List<CarType> cartypes = query.getResultList();
        return cartypes;
    }

    public static CarType retrieve(EntityManager em, Integer cartypeId) {
        CarType cartype = em.find( CarType.class, cartypeId);
        if ( cartype != null) {
            System.out.println( "CarType.retrieve: loaded cartype " + cartype);
        }
        return cartype;
    }
    /**
     * Update an Author instance
     *
     *
     * @param em
     *          reference to the entity manager
     * @param ut
     *          reference to the user transaction
     * @param cartypeId
     *          the cartypeid value of the cartype to update
     * @param name
     *          the name value of the author to update
     * @param cartypedCars
     *          the cartypedcars value of the cartype to update
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public static void update( EntityManager em, UserTransaction ut,
                               Integer cartypeId, String name, Set<Car> cartypedCars)
            throws NotSupportedException, SystemException, IllegalStateException,
            SecurityException, HeuristicMixedException,
            HeuristicRollbackException, RollbackException, javax.transaction.RollbackException {
        ut.begin();
        CarType carType = em.find( CarType.class, cartypeId);
        if ( carType == null) {
            throw new EntityNotFoundException( "The cartype with ID = " + carType
                    + " was not found!");
        }
        // delete the references of the updated author from all the cars cartyped
        // by this cartype before the current update operation
        for ( Car c : carType.getCartypedCars()) {
            c.removeCarType( carType);
            em.merge( c);
        }
        if ( name != null && !name.equals( carType.name)) {
            carType.setName( name);
        }
        if ( cartypedCars != null && !cartypedCars.equals( carType.cartypedCars)) {
            carType.setCartypedCars( cartypedCars);
        }
        // update the ManyToMany reference: the cartypes for the car
        for ( Car c : cartypedCars) {
            c.addCarType( carType);
            em.merge( c);
        }
        ut.commit();
        System.out.println( "Cartype.update: the Cartype " + carType + " was updated.");
    }
    /**
     * Delete an Author instance
     *
     * @param em
     *          reference to the entity manager
     * @param ut
     *          reference to the user transaction
     * @param cartypeId
     *          the personId value of the author to delete
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public static void destroy( EntityManager em, UserTransaction ut,
                                Integer cartypeId) throws NotSupportedException, SystemException,
            IllegalStateException, SecurityException, HeuristicMixedException,
            HeuristicRollbackException, RollbackException, javax.transaction.RollbackException {
        ut.begin();
        CarType carType = em.find( CarType.class, cartypeId);
        carType.setCartypedCars( null);
        em.remove( carType);
        ut.commit();
        System.out.println( "Cartype.destroy: the cartype " + carType + " was deleted.");
    }
    public static void clearData( EntityManager em,
                                  UserTransaction ut) throws Exception {
        ut.begin();
        Query deleteStatement = em.createQuery( "DELETE FROM CarType");
        deleteStatement.executeUpdate();
        ut.commit();
    }
//    public static void generateTestData( EntityManager em,
//                                         UserTransaction ut) throws Exception {
//        CarType cartype = null;
//        CarType.clearData( em, ut);  // first clear the books table
//        ut.begin();
//        cartype = new CarType( 1, "Compact", new HashSet<Car>());
//        em.persist( cartype);
//        cartype = new CarType( 2, "Small", new HashSet<Car>());
//        em.persist( cartype);
//        cartype = new CarType( 3, "Medium", new HashSet<Car>());
//        em.persist( cartype);
//        ut.commit();
//    }

}

