package dresden23.model.converter;

import dresden23.ctrl.cartypeController;
import dresden23.model.CarType;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

@FacesConverter( value="dresden23.model.converter.CarTypeConverter")
public class CarTypeConverter implements Converter {
  @Override
  public Object getAsObject( FacesContext context, UIComponent component,
      String value) {
    cartypeController ac = FacesContext
        .getCurrentInstance()
        .getApplication()
        .evaluateExpressionGet( context, "#{cartypeCtrl}",
            cartypeController.class);
    EntityManager em = ac.getEntityManager();
    if ( value == null) {
      return null;
    }
    return em.find( CarType.class, Integer.parseInt( value));
  }

  @Override
  public String getAsString( FacesContext context, UIComponent component,
      Object value) {
    if ( value == null) {
      return null;
    } else if ( value instanceof CarType) {
      return Integer.toString( ((CarType) value).getCartypeId());
    }
    return null;
  }
}