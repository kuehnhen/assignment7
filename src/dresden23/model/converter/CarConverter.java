package dresden23.model.converter;

import dresden23.ctrl.carController;
import dresden23.model.Car;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

@FacesConverter( value="dresden23.model.converter.CarConverter")
public class CarConverter implements Converter {
  @Override
  public Object getAsObject( FacesContext context, UIComponent component,
      String value) {
    carController ac = FacesContext
        .getCurrentInstance()
        .getApplication()
        .evaluateExpressionGet( context, "#{carCtrl}",
            carController.class);
    EntityManager em = ac.getEntityManager();
    if ( value == null) {
      return null;
    }
    return em.find( Car.class, Integer.parseInt(value));
  }

  @Override
  public String getAsString( FacesContext context, UIComponent component,
      Object value) {
    if ( value == null) {
      return null;
    } else if ( value instanceof Car) {
      return Integer.toString(((Car) value).getCarId());
    }
    return null;
  }
}