package dresden23.model.converter;

import dresden23.ctrl.offeroftheweekController;
import dresden23.model.OfferOfTheWeek;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

@FacesConverter( value="dresden23.model.converter.OfferOfTheWeekConverter")
public class OfferOfTheWeekConverter implements Converter {
  @Override
  public Object getAsObject( FacesContext context, UIComponent component,
      String value) {
    offeroftheweekController ac = FacesContext
        .getCurrentInstance()
        .getApplication()
        .evaluateExpressionGet( context, "#{offeroftheweekCtrl}",
            offeroftheweekController.class);
    EntityManager em = ac.getEntityManager();
    if ( value == null) {
      return null;
    }
    return em.find( OfferOfTheWeek.class, Integer.parseInt(value));
  }

  @Override
  public String getAsString( FacesContext context, UIComponent component,
      Object value) {
    if ( value == null) {
      return null;
    } else if ( value instanceof OfferOfTheWeek) {
      return Integer.toString(((OfferOfTheWeek) value).getOfferId());
    }
    return null;
  }
}