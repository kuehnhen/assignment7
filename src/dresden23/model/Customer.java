package dresden23.model;

import com.sun.istack.internal.NotNull;

import javax.persistence.*;
import javax.transaction.UserTransaction;
import java.util.List;

@Entity
@Table(name = "customers")

public class Customer {
    @NotNull @Id private int customerId;
    @NotNull private String firstname;
    @NotNull private String surname;
    @NotNull private String address;
    // default consructor (required for entity classes)
    public Customer() {}
    // constructor
    public Customer (int customerId, String firstname, String surname, String address) {
        this.setCustomerId(customerId);
        this.setFirstName(firstname);
        this.setSurName(surname);
        this.setAddress(address);
    }
    public int getCustomerId() {
        return  customerId;
    }
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    public String getFirstName() {
        return firstname;
    }
    public void setFirstName(String firstname) {
        this.firstname = firstname;
    }
    public String getSurName() {
        return surname;
    }
    public void setSurName(String surname) {
        this.surname = surname;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    // CRUD data management methods
    public static void create(EntityManager em, UserTransaction ut,
                              int customerId, String firstname, String surname, String address)
            throws Exception {
        ut.begin();
        Customer customer = new Customer( customerId, firstname, surname, address);
        em.persist( customer);
        ut.commit();
    }

    public static List<Customer> retrieveAll(EntityManager em) {
        Query query = em.createQuery( "SELECT c FROM Customer c", Customer.class);
        List<Customer> customers = query.getResultList();
        return customers;
    }

    public static Customer retrieve(EntityManager em, int customerId) {
        Customer customer = em.find( Customer.class, customerId);
        if ( customer != null) {
            System.out.println( "Customer.retrieve: loaded customer " + customer);
        }
        return customer;
    }
    public static void update( EntityManager em,
                               UserTransaction ut, int customerId, String firstname, String surname, String address
                               ) throws Exception {
        ut.begin();
        Customer customer = em.find( Customer.class, customerId);
        if (!firstname.equals( customer.getFirstName())) customer.setFirstName( firstname);
        if (!firstname.equals( customer.getSurName())) customer.setSurName( firstname);
        ut.commit();
    }
    public static void delete( EntityManager em,
                               UserTransaction ut, int customerId) throws Exception {
        ut.begin();
        Customer customer = em.find( Customer.class, customerId);
        em.remove( customer);
        ut.commit();
    }
    public static void clearData( EntityManager em,
                                  UserTransaction ut) throws Exception {
        ut.begin();
        Query deleteStatement = em.createQuery( "DELETE FROM Customer");
        deleteStatement.executeUpdate();
        ut.commit();
    }
    public static void generateTestData( EntityManager em,
                                         UserTransaction ut) throws Exception {
        Customer customer = null;
        Customer.clearData( em, ut);  // first clear the books table
        ut.begin();
        customer = new Customer(1,"Hans", "Schmidt", "Dummyaddress");
        em.persist( customer);
        customer = new Customer(1,"Walter", "Wurst", "Dummyaddress");
        em.persist( customer);
        customer = new Customer(1,"Ivan", "Drago", "Dummyaddress");
        em.persist( customer);
        ut.commit();
    }


}
