package dresden23.model.exception;

public class OtherConstraintViolation extends ConstraintViolation {
  private static final long serialVersionUID = -5676228873004346098L;

  public OtherConstraintViolation( String message) {
    super( message);
  }
}