package dresden23.model.exception;

public class RangeConstraintViolation extends ConstraintViolation {
  private static final long serialVersionUID = 7968481943510109702L;

  public RangeConstraintViolation( String message) {
    super( message);
  }
}