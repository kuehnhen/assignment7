package dresden23.model.exception;

public class IntervalConstraintViolation extends ConstraintViolation {
  private static final long serialVersionUID = -4454924079346491938L;

  public IntervalConstraintViolation( String message) {
    super( message);
  }
}