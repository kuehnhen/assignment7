package dresden23.model.exception;

public class PatternConstraintViolation extends ConstraintViolation {
  private static final long serialVersionUID = 6674848455810006490L;

  public PatternConstraintViolation( String message) {
    super( message);
  }
}