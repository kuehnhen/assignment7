package dresden23.model.exception;

public class MandatoryValueConstraintViolation extends ConstraintViolation {
  private static final long serialVersionUID = 8806150087657774926L;

  public MandatoryValueConstraintViolation( String message) {
    super( message);
  }
}