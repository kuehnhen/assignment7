package dresden23.model.exception;

public class UniquenessConstraintViolation extends ConstraintViolation {
  private static final long serialVersionUID = 5613810706558655116L;

  public UniquenessConstraintViolation( String message) {
    super( message);
  }
}