package dresden23.model.exception;

public class ConstraintViolation extends Exception {
  private static final long serialVersionUID = -1245950465346945734L;

  public ConstraintViolation( String message) {
    super( message);
  }
}