package dresden23.model;

import com.sun.istack.internal.NotNull;
import dresden23.model.exception.IntervalConstraintViolation;
import dresden23.model.exception.MandatoryValueConstraintViolation;
import dresden23.model.exception.UniquenessConstraintViolation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.RollbackException;
import javax.persistence.*;
import javax.transaction.*;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Entity //persistance storage
@Table(name ="rentals") //name of the table storing data
@RequestScoped
@ManagedBean( name="rental")

public class Rental {
    @NotNull @Id private Integer rentalId;
    @NotNull private Date rentaldate;
    @NotNull private Date rentalfrom;
    @NotNull private Date rentalto;
    @NotNull private Integer discount;
    @NotNull private boolean deliverytothepoint;
    private String deliveryaddress;
    @NotNull private Integer total;
    @ManyToOne( fetch=FetchType.EAGER)
    private Car car;
    @ManyToOne ( fetch=FetchType.EAGER)
    private PickUpPoint pickuppoint;
    @ManyToOne ( fetch=FetchType.EAGER)
    private Customer customer;
    // default constructor (required for entity classes)
    public Rental() {}
    // constructor
    public Rental ( Integer rentalId, Date rentaldate, Date rentalfrom, Date rentalto, Integer discount,
                    boolean deliverytothepoint, String deliveryaddress, Integer total, Car car, PickUpPoint pickuppoint,
                    Customer customer) {
        this.setRentalId (rentalId);
        this.setRentaldate(rentaldate);
        this.setRentalfrom (rentalfrom);
        this.setRentalto (rentalto);
        this.setDiscount(discount);
        this.setDeliveryToThePoint(deliverytothepoint);
        this.setDeliveryaddress(deliveryaddress);
        this.setTotal(total);
        this.setCar(car);
        this.setPickuppoint(pickuppoint);
        this.setCustomer(customer);
    }
    public Integer getRentalId() {
        return rentalId;
    }
    public void setRentalId(Integer rentalId) {
        this.rentalId = rentalId;
    }
    public Date getRentaldate() {
        return rentaldate;
    }
    public void setRentaldate(Date rentaldate) {
        this.rentaldate = rentaldate;
    }
    public Date getRentalfrom() {
        return rentalfrom;
    }
    public void setRentalfrom(Date rentalfrom) {
        this.rentalfrom = rentalfrom;
    }
    public Date getRentalto() {
        return rentalto;
    }
    public void setRentalto(Date rentalto) {
        this.rentalto = rentalto;
    }
    public Integer getDiscount() {
        return discount;
    }
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }
    public boolean getDeliveryToThePoint() {
        return deliverytothepoint;
    }
    public void setDeliveryToThePoint(boolean deliverytothepoint) {
        this.deliverytothepoint = deliverytothepoint;
    }
    public String getDeliveryaddress() {
        return deliveryaddress;
    }
    public void setDeliveryaddress(String deliveryaddress) {
        this.deliveryaddress = deliveryaddress;
    }
    public Integer getTotal() {
        return total;
    }
    public void setTotal(Integer total) {
        this.total = total;
    }
    public Car getCar() {
        return car;
    }
    public void setCar(Car car) {
        this.car = car;
    }
    public PickUpPoint getPickuppoint() {
        return pickuppoint;
    }
    public void setPickuppoint(PickUpPoint pickuppoint) {
        this.pickuppoint = pickuppoint;
    }
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * Create a human readable serialization.
     */
    public String toString() {
        String result = "{ RentalID: " + this.rentalId + ", Date:'" + this.rentaldate +", RentalFrom " + this.rentalfrom
                + "RentalTo: " + this.rentalto +"Discount: " +this.discount+ "DeliveratothePoint: "
                +this.deliverytothepoint +"DeliveryAddress:" +this.deliveryaddress + "Total:" +this.total
                + "Car: "+ this.car + "PickupPoint: "+ this.pickuppoint + "Customer: " +this.customer;
        return result;
    }

    @Override
    public boolean equals( Object obj) {
        if (obj instanceof Rental) {
            Rental rental = (Rental) obj;
            return ( this.rentalId.equals( rental.rentalId));
        } else return false;
    }

    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws UniquenessConstraintViolation
     */
    public static void checkRentalIdAsId( EntityManager em, Integer rentalId)
            throws UniquenessConstraintViolation,
            IntervalConstraintViolation {
        if (rentalId < 0) {
            throw new IntervalConstraintViolation(
                    "RentalId must be at least 0");
        } else {
            Rental rental = Rental.retrieve( em, rentalId);
            // car was found, uniqueness constraint validation failed
            if ( rental != null) {
                throw new UniquenessConstraintViolation(
                        "There is already a Rental record with this rentalId!");
            }
        }
    }

    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws IntervalConstraintViolation
     */
    public static void checkRentalDate( EntityManager em, Date rentaldate)
            throws IntervalConstraintViolation, ParseException {
        if ( rentaldate.before(new Date())) {
            throw new IntervalConstraintViolation(
                    "The rental cannot be in the past");
        }
    }
    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws IntervalConstraintViolation
     */
    public static void checkRentalFrom( EntityManager em, Date rentalfrom)
            throws IntervalConstraintViolation, ParseException {
        if ( rentalfrom.before(new Date())) {
            throw new IntervalConstraintViolation(
                    "The rental cannot be in the past");
        }
    }

    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws IntervalConstraintViolation
     */
    public static void checkRentalTo( EntityManager em, Date rentalto)
            throws IntervalConstraintViolation, ParseException {
        if ( rentalto.before(new Date())) {
            throw new IntervalConstraintViolation(
                    "The rental cannot be in the past");
        }
    }

    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws IntervalConstraintViolation
     */
    public static void checkDiscount( EntityManager em, Integer discount)
            throws IntervalConstraintViolation {
        if ( discount < 0) {
            throw new IntervalConstraintViolation(
                    "Discount cant be lower than zero");
        }
    }

    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws IntervalConstraintViolation
     */
    public static void checkDeliverytothepoint( EntityManager em, boolean deliverytothepoint)
            throws IntervalConstraintViolation {
        if ( deliverytothepoint) {
            throw new IntervalConstraintViolation(
                    "Discount cant be lower than zero");
        }
    }


    public static void checkDeliveryaddress( EntityManager em, String deliveryaddress)
            throws MandatoryValueConstraintViolation {
        if (deliveryaddress == null) {
            throw new MandatoryValueConstraintViolation(
                    "Car Name is mandatory");
        }
    }

    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws IntervalConstraintViolation
     */
    public static void checkTotal( EntityManager em, Integer total)
            throws IntervalConstraintViolation {
        if ( total < 0) {
            throw new IntervalConstraintViolation(
                    "The car has rentalto have at least 1hp power");
        }
    }


    // CRUD data management methods
    public static void create(EntityManager em, UserTransaction ut,
                              Integer rentalId, Date rentaldate, Date rentalfrom, Date rentalto, Integer discount,
                              boolean deliverytothepoint, String deliveryaddress, Integer total, Car car,
                              PickUpPoint pickuppoint, Customer customer)
            throws Exception {
        ut.begin();
        Rental rental = new Rental( rentalId, rentaldate, rentalfrom, rentalto, discount, deliverytothepoint,
                deliveryaddress, total, car, pickuppoint, customer);
        em.persist( rental);
        ut.commit();
    }

    public static List<Rental> retrieveAll(EntityManager em) {
        Query query = em.createQuery( "SELECT r FROM Rental r", Rental.class);
        List<Rental> rentals = query.getResultList();
        return rentals;
    }

    public static Rental retrieve(EntityManager em, Integer rentalId) {
        Rental rental = em.find( Rental.class, rentalId);
        if ( rental != null) {
            System.out.println( "Rental.retrieve: loaded rental " + rental);
        }
        return rental;
    }
    public static void update( EntityManager em,
                               UserTransaction ut, Integer rentalId, Date rentaldate, Date rentalfrom, Date rentalto,
                               Integer discount, boolean deliverytothepoint, String deliveryaddress, Integer total,
                               Car car, PickUpPoint pickuppoint, Customer customer)
            throws NotSupportedException, SystemException, IllegalStateException,
            SecurityException, HeuristicMixedException, HeuristicRollbackException,
            RollbackException, javax.transaction.RollbackException {
        ut.begin();
        Rental rental = em.find( Rental.class, rentalId);
        if ( rental == null) {
            throw new EntityNotFoundException( "The Rental with ID = " + rentalId
                    + " was not found!");
        }
        Car oldcar = rental.getCar();
        PickUpPoint oldpickuppoint = rental.getPickuppoint();
        Customer oldcustomer = rental.getCustomer();
        if (rentaldate != rental.getRentaldate()) rental.setRentaldate(rentaldate);
        if (rentalfrom != rental.getRentalfrom()) rental.setRentalfrom(rentalfrom);
        if (rentalto != rental.getRentalto()) rental.setRentalto(rentalto);
        if (!discount.equals(rental.getDiscount())) rental.setDiscount(discount);
        if (deliverytothepoint != rental.getDeliveryToThePoint()) rental.setDeliveryToThePoint(deliverytothepoint);
        if (!deliveryaddress.equals( rental.getDeliveryaddress())) rental.setDeliveryaddress( deliveryaddress);
        if (!total.equals(rental.getTotal())) rental.setTotal(total);
        if ( car != null && !car.equals( rental.car)) {
            rental.setCar( car);
        }
        if ( pickuppoint != null && !pickuppoint.equals( rental.pickuppoint)) {
            rental.setPickuppoint( pickuppoint);
        }
        if ( customer != null && !customer.equals( rental.customer)) {
            rental.setCustomer( customer);
        }
        // merge managed entities in the memory, also update the cached versions
        // and the entity changes are reflected next time when used
        if ( oldcar != null) {
            em.merge( oldcar);
        }
        if ( car != null) {
            em.merge( car);
        }
        if ( oldpickuppoint != null) {
            em.merge( oldpickuppoint);
        }
        if ( pickuppoint != null) {
            em.merge( pickuppoint);
        }
        if ( oldcustomer != null) {
            em.merge( oldcustomer);
        }
        if ( customer != null) {
            em.merge( customer);
        }
        ut.commit();
    }
    public static void delete( EntityManager em,
                               UserTransaction ut, Integer rentalId) throws Exception {
        ut.begin();
        Rental rental = em.find( Rental.class, rentalId);
        em.remove( rental);
        ut.commit();
    }
    public static void clearData( EntityManager em,
                                  UserTransaction ut) throws Exception {
        ut.begin();
        Query deleteStatement = em.createQuery( "DELETE FROM Rental");
        deleteStatement.executeUpdate();
        ut.commit();
    }
//    public static void generateTestData( EntityManager em,
//                                         UserTransaction ut) throws Exception {
//        Rental rental = null;
//        Rental.clearData( em, ut);  // first clear the books table
//        ut.begin();
//        rental = new Rental(1,new Date(2008), new Date(2008),new Date(2008), 75, true, "Dummyaddress", 50);
//        em.persist( rental);
//        rental = new Rental(2,new Date(2008), new Date(2008),new Date(2008), 75, true, "Dummyaddress", 50);
//        em.persist( rental);
//        rental = new Rental(3,new Date(2008), new Date(2008),new Date(2008), 75, true, "Dummyaddress", 50);
//        em.persist( rental);
//        ut.commit();
//    }


}
