package dresden23.model;

import com.sun.istack.internal.NotNull;
import dresden23.model.exception.IntervalConstraintViolation;
import dresden23.model.exception.MandatoryValueConstraintViolation;
import dresden23.model.exception.UniquenessConstraintViolation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.*;
import javax.persistence.RollbackException;
import javax.transaction.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "offeroftheweeks")
@RequestScoped
@ViewScoped
@ManagedBean( name="offeroftheweek")

public class OfferOfTheWeek {
    @NotNull @Id private Integer offerId;
    @NotNull private String name;
    @NotNull private Date datefrom;
    @NotNull private Date dateto;
    @NotNull private Integer discount;
    @OneToMany( fetch=FetchType.LAZY)
    private Set<Car> cars;
    //default constructor
    public OfferOfTheWeek() {}
    //constructor
    public OfferOfTheWeek( Integer offerId, String name, Date datefrom, Date dateto, Integer discount, Set<Car> cars) {
        this.setOfferId(offerId);
        this.setName(name);
        this.setDateFrom(datefrom);
        this.setDateTo(dateto);
        this.setDiscount(discount);
        this.setCars(cars);
    }
    public Integer getOfferId() {
        return offerId;
    }
    public void setOfferId(Integer offerId) {
        this.offerId = offerId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Date getDateFrom() {
        return datefrom;
    }
    public void setDateFrom(Date datefrom) {
        this.datefrom = datefrom;
    }
    public Date getDateTo() {
        return dateto;
    }
    public void setDateTo(Date dateto) {
        this.dateto = dateto;
    }
    public Integer getDiscount() {
        return discount;
    }
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Set<Car> getCars() {
        return cars;
    }

    public void setCars( Set<Car> cars) {
        this.cars = cars;
    }

    /**
     * Return the serialized car names used in the JSP views.
     *
     * @return a serialized car names for this OfferofTheWeek.
     */
    public String getCarNames() {
        String result = "";
        int i = 0, n = 0;
        if ( this.cars != null) {
            n = this.cars.size();
            for ( Car car : this.cars) {
                result += car.getName();
                if ( i < n - 1) {
                    result += ", ";
                }
                i++;
            }
        }
        return result;
    }

    /**
     * Create a human readable serialization.
     */
    public String toString() {
        String result = "{ OfferId: '" + this.offerId + "', Name:'" + this.name
                + "', datefrom: " + this.datefrom + ", dateto: "+ this.dateto + " discount: "+ this.discount;
        result += ", cars: [";
        if ( this.getCars() != null) {
            int i = 0, n = this.getCars().size();
            for ( Car c : this.getCars()) {
                result += "'" + c.getName() + "'";
                if ( i < n - 1) {
                    result += ", ";
                }
                i++;
            }
        }
        result += "}";
        return result;
    }


    @Override
    public boolean equals( Object obj) {
        if (obj instanceof Car) {
            OfferOfTheWeek offer = (OfferOfTheWeek) obj;
            return ( this.offerId.equals( offer.offerId));
        } else return false;
    }


    // CRUD data management methods
    public static void create( EntityManager em, UserTransaction ut, Integer offerId, String name, Date datefrom,
                               Date dateto, Integer discount, Set<Car> cars)
            throws NotSupportedException, SystemException, IllegalStateException,
            SecurityException, HeuristicMixedException, HeuristicRollbackException,
            RollbackException, EntityExistsException, javax.transaction.RollbackException {
        ut.begin();
        OfferOfTheWeek offer = new OfferOfTheWeek( offerId, name, datefrom, dateto, discount, cars);
        em.persist( offer);
        if ( cars != null) {
            for ( Car c : cars) {
                em.merge( c);
            }
        }
        ut.commit();
        System.out.println( "Offer.add: the Offer " + offer + " was saved.");
    }

    public static List<OfferOfTheWeek> retrieveAll(EntityManager em) {
        Query query = em.createQuery( "SELECT o FROM OfferOfTheWeek o", OfferOfTheWeek.class);
        List<OfferOfTheWeek> offers = query.getResultList();
        return offers;
    }

    public static OfferOfTheWeek retrieve(EntityManager em, Integer offerId) {
        OfferOfTheWeek offer = em.find( OfferOfTheWeek.class, offerId);
        if ( offer != null) {
            System.out.println( "OfferOfTheWeek.retrieve: loaded offer " + offer);
        }
        return offer;
    }

    public static void update( EntityManager em, UserTransaction ut, Integer offerId, String name, Date datefrom, Date dateto,
                               Integer discount, Set<Car> cars)
            throws NotSupportedException, SystemException, IllegalStateException,
            SecurityException, HeuristicMixedException, HeuristicRollbackException,
            RollbackException, javax.transaction.RollbackException {
        ut.begin();
        OfferOfTheWeek offer = em.find(OfferOfTheWeek.class, offerId);
        if (offer == null) {
            throw new EntityNotFoundException("The Offer with OfferId = " + offer
                    + " was not found!");
        }
        Set<Car> oldCars = offer.getCars();
        if (name != null && !name.equals(offer.name)) {
            offer.setName(name);
        }
        if (datefrom != null && !datefrom.equals(offer.datefrom)) {
            offer.setDateFrom(datefrom);
        }
        if (dateto != null && !dateto.equals(offer.dateto)) {
            offer.setDateTo(dateto);
        }
        if (discount != null && !discount.equals(offer.discount)) {
            offer.setDiscount(discount);
        }
        if (cars != null && !cars.equals(offer.cars)) {
            offer.setCars(cars);
        }
        // merge managed entities in the memory, also update the cached versions
        // and the entity changes are reflected next time when used
        if (oldCars != null) {
            for (Car c : oldCars) {
                em.merge(c);
            }
        }
        if (cars != null) {
            for (Car c : cars) {
                em.merge(c);
            }
        }
        ut.commit();
        System.out.println("Offer.update: the offer " + offer + " was updated.");
    }

    public static void delete( EntityManager em,
                               UserTransaction ut, Integer offerId) throws Exception {
        ut.begin();
        OfferOfTheWeek offer = em.find( OfferOfTheWeek.class, offerId);
        em.remove( offer);
        ut.commit();
    }
    public static void clearData( EntityManager em,
                                  UserTransaction ut) throws Exception {
        ut.begin();
        Query deleteStatement = em.createQuery( "DELETE FROM OfferOfTheWeek ");
        deleteStatement.executeUpdate();
        ut.commit();
    }

    /**
     * Check for the offerId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given offerId value.
     *
     * @throws UniquenessConstraintViolation
     */
    public static void checkOfferIdAsId( EntityManager em, Integer offerId)
            throws UniquenessConstraintViolation,
            IntervalConstraintViolation {
        if (offerId < 0) {
            throw new IntervalConstraintViolation(
                    "OfferId must be at least 0");
        } else {
            Car car = Car.retrieve( em, offerId);
            // car was found, uniqueness constraint validation failed
            if ( car != null) {
                throw new UniquenessConstraintViolation(
                        "There is already a offer record with this offerId!");
            }
        }
    }

    public static void checkName( EntityManager em, String name)
            throws MandatoryValueConstraintViolation {
        if (name == null) {
            throw new MandatoryValueConstraintViolation(
                    "Offer Name is mandatory");
        }
    }

    public static void checkDateFrom( EntityManager em, Date dateFrom)
            throws MandatoryValueConstraintViolation {
        if (dateFrom == null) {
            throw new MandatoryValueConstraintViolation(
                    "Date From is mandatory");
        }
    }

    public static void checkDateTo( EntityManager em, Date dateTo)
            throws MandatoryValueConstraintViolation {
        if (dateTo == null) {
            throw new MandatoryValueConstraintViolation(
                    "Date To is mandatory");
        }
    }

    public static void checkDiscount( EntityManager em, Integer discount)
            throws MandatoryValueConstraintViolation {
        if (discount == null) {
            throw new MandatoryValueConstraintViolation(
                    "Discount is mandatory");
        }
    }

//    public static void generateTestData( EntityManager em,
//                                         UserTransaction ut) throws Exception {
//        OfferOfTheWeek offer = null;
//        OfferOfTheWeek.clearData( em, ut);  // first clear the books table
//        ut.begin();
//        offer = new OfferOfTheWeek(1,"Offer 1", new Date(2008), new Date(2008), 5000);
//        em.persist( offer);
//        offer = new OfferOfTheWeek(2,"Offer 2", new Date(2008), new Date(2008), 5000);
//        em.persist( offer);
//        offer = new OfferOfTheWeek(3,"Offer 3", new Date(2008), new Date(2008), 6000);
//        em.persist( offer);
//        ut.commit();
//    }

}
