package dresden23.model;

// for date datatype

import dresden23.model.exception.IntervalConstraintViolation;
import dresden23.model.exception.MandatoryValueConstraintViolation;
import dresden23.model.exception.UniquenessConstraintViolation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.persistence.RollbackException;
import javax.persistence.*;
import javax.transaction.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import com.sun.istack.internal.NotNull;
/* The annotation @Entity designates a class as an entity class implying that the instances of this class will be stored
 persistently. --> Java Persistence API (JPA)
 */
@Entity
@Table( name="cars")
/*
4.4. Define the managed beans needed in facelets


JavaBean classes, including entity classes, can be used for creating 'managed beans' with the help of the @ManagedBean
 annotation, which allows defining the name of a variable for accessing the created bean in the view code, typically
 in an EL expression. In our example app, we want to access a Car bean as well as a CarController bean, therefore both
  classes have to be annotated as follows:

  Notice how a lifetime scope can be specified for a managed bean with a scope annotation. In our example the book bean
   is @RequestScoped, this means the instance exists as long as the HTTP request and the associated response are being
   processed. The bookCtrl bean is @SessionScoped, which means it is created when the session starts, and destroyed when
    the session is closed. Other scopes are available, but in our example we only need these two.

 */
@RequestScoped @ManagedBean( name="car") @ViewScoped

public class Car {
    //Mandatory, ID
    @Id
    @NotNull ( message="A carId is required!")
    private Integer carId;
    @Column( nullable=false)
    @NotNull ( message="A Car name is required!")
    private String name;
    @Column( nullable=false)
    @NotNull ( message="A car ProductionYear is required!")
    private Date productionYear;
    @Column( nullable=false)
    @NotNull ( message="A value for horsepower is required!")
    private Integer power;
    @Column( nullable=false)
    @NotNull ( message="A Status for the car is required!")
    @Enumerated( EnumType.STRING)
    private StatusEL status;
    @Column( nullable=false)
    @NotNull ( message="A Price for the car is required!")
    @Min( value=0, message="Price must be > 0!")
    private Integer price;
    @ManyToMany( fetch=FetchType.EAGER)
    @JoinTable( name="cars_cartypes",
            joinColumns={ @JoinColumn( name="car_carid") },
            inverseJoinColumns = { @JoinColumn( name="cartype_cartypeid") })
    private Set<CarType> cartypes;
    // default constructor (required for entity classes)
    public Car() {}
    // constructor
    public Car(Integer carId, String name, Date productionYear, Integer power, StatusEL status, Integer price,
               Set<CarType> cartypes) {
        this.setCarId(carId);
        this.setName(name);
        this.setProductionYear(productionYear);
        this.setPower(power);
        this.setStatus(status);
        this.setPrice(price);
        this.setCarTypes(cartypes);
    }
    // getter and setter methods
    public Integer getCarId() {
        return carId;
    }
    public void setCarId(Integer carId) {
        this.carId = carId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Date getProductionYear() {
        return productionYear;
    }
    public void setProductionYear(Date productionYear) {
        this.productionYear = productionYear;
    }
    public Integer getPower() {
       return power;
    }
    public void setPower(Integer power) {
        this.power = power;
    }
    public StatusEL getStatus() {
        return status;
    }
    public void setStatus(StatusEL status) {
        this.status = status;
    }
    public Integer getPrice() {
        return price;
    }
    public void setPrice(Integer price) {
        this.price = price;
    }

    public Set<CarType> getCarTypes() {
        return cartypes;
    }

    public void setCarTypes( Set<CarType> cartypes) {
        Set<CarType> oldCarTypes = this.cartypes != null ? new HashSet<CarType>(
                this.cartypes) : null;
        // remove old cartypes
        if ( oldCarTypes != null) {
            for ( CarType c : oldCarTypes) {
                this.removeCarType( c);
            }
        }
        // add new cartypes
        if ( cartypes != null) {
            for ( CarType c : cartypes) {
                this.addCarType( c);
            }
        } else {
            if ( this.cartypes != null) {
                this.cartypes.clear();
            }
        }
    }

    public void addCarType( CarType cartype) {
        if ( this.cartypes == null) {
            this.cartypes = new HashSet<CarType>();
        }
        if ( !this.cartypes.contains( cartype)) {
            this.cartypes.add( cartype);
            cartype.addCartypedCar( this);
        }
    }

    public void removeCarType( CarType cartype) {
        if ( this.cartypes != null && cartype != null
                && this.cartypes.contains( cartype)) {
            this.cartypes.remove( cartype);
            cartype.removeCartypedCar( this);
        }
    }

    /**
     * Return the serialized cartype names used in the JSF views.
     *
     * @return serialized cartype names for this book.
     */
    public String getCarTypeNames() {
        String result = "";
        Integer i = 0, n = 0;
        if ( this.cartypes != null) {
            n = this.cartypes.size();
            for ( CarType cartype : this.cartypes) {
                result += cartype.getName();
                if ( i < n - 1) {
                    result += ", ";
                }
                i++;
            }
        }
        return result;
    }


//    /**
//     * Create a human readable serialization.
//     */
//    public String toString() {
//        return "{ carId: '" + this.carId + "', Name:'" + this.name + "', ProductionproductionYear: '"
//                + this.productionYear + "', Power: '" +this.power + "' Status: '"+ this.status + "' price: '"
//                + this.price + "}";
//    }

    /**
     * Create a human readable serialisation.
     *
     */
    public String toString() {
        String result = "{ carid: '" + this.carId + "', name:'" + this.name
                + "', productionYear: " + this.productionYear + ", publisher: "
                + "', Power: '" +this.power + "' Status: '"+ this.status + "' price: '" + this.price
                + ", cartypes: [";
        if ( this.cartypes != null) {
            Integer i = 0, n = this.cartypes.size();
            for ( CarType c : this.cartypes) {
                result += "'" + c.getName() + "'";
                if ( i < n - 1) {
                    result += ", ";
                }
                i++;
            }
        }
        result += "]}";
        return result;
    }

    @Override
    public boolean equals( Object obj) {
        if (obj instanceof Car) {
            Car car = (Car) obj;
            return ( this.carId.equals( car.carId));
        } else return false;
    }


    /**
     * Create and return the set of items containing the statuses which are
     * available for the car. The available languages are the literals of
     * LanguageEL enumeration.
     *
     * @return the set of status items
     */
    public SelectItem[] getStatusItems() {
        SelectItem[] items = new SelectItem[StatusEL.values().length];
        Integer i = 0;
        for ( StatusEL status : StatusEL.values()) {
            items[i++] = new SelectItem( status.name(), status.getLabel());
        }
        return items;
    }



    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws UniquenessConstraintViolation
     */
    public static void checkCarIdAsId( EntityManager em, Integer carId)
            throws UniquenessConstraintViolation,
            IntervalConstraintViolation {
        if (carId < 0) {
            throw new IntervalConstraintViolation(
                    "CarId must be at least 0");
        } else {
            Car car = Car.retrieve( em, carId);
            // car was found, uniqueness constraint validation failed
            if ( car != null) {
                throw new UniquenessConstraintViolation(
                    "There is already a car record with this carId!");
            }
        }
    }

    public static void checkName( EntityManager em, String name)
            throws MandatoryValueConstraintViolation {
        if (name == null) {
            throw new MandatoryValueConstraintViolation(
                    "Car Name is mandatory");
        }
    }



    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws IntervalConstraintViolation
     */
    public static void checkProductionYear( EntityManager em, Date productionYear)
            throws IntervalConstraintViolation, ParseException {
        SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
        Date datecheck = date.parse("01/01/1886");
        if ( productionYear.before(datecheck)) {
            throw new IntervalConstraintViolation(
                    "The car cannot be produced before 1886");
        }
    }

    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws IntervalConstraintViolation
     */
    public static void checkPower( EntityManager em, Integer power)
            throws IntervalConstraintViolation {
        if ( power < 1) {
            throw new IntervalConstraintViolation(
                    "The car has to have at least 1hp power");
        }
    }

    /**
     * Check for the carId uniqueness constraint by verifying the existence in the
     * database of a car entry for the given carId value.
     *
     * @throws IntervalConstraintViolation
     */
//    public static void checkStatus( EntityManager em, StatusEL status)
//            throws IntervalConstraintViolation {
//        if ( status > 1) {
//            throw new IntervalConstraintViolation(
//                    "The car has to have at least 1hp power");
//        }
//    }

    public static void checkPrice( EntityManager em, Integer price)
            throws IntervalConstraintViolation {
        if ( price < 0) {
            throw new IntervalConstraintViolation(
                    "Price cant be negative");
        }
    }


    // CRUD data management methods
//    public static void create( EntityManager em, UserTransaction ut,
//                               Integer carId, String name, Date productionYear, Integer power, StatusEL status, Integer price)
//    throws Exception {
//        ut.begin();
//        Car car = new Car( carId, name, productionYear, power, status, price);
//        em.persist( car);
//        ut.commit();
//    }
    /**
     * Create a Book instance.
     *
     * @param em
     *          reference to the entity manager
     * @param ut
     *          reference to the user transaction
     * @param carId
     *          the ISBN value of the book to create
     * @param name
     *          the title value of the book to create
     * @param productionYear
     *          the year value of the book to create
     * @param power
     *          the publisher value of the book to create
     * @param status
     *          the authors for the book to create
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public static void create( EntityManager em, UserTransaction ut, Integer carId,
                            String name, Date productionYear, Integer power, StatusEL status, Integer price,
                               Set<CarType> cartypes)
            throws NotSupportedException, SystemException, IllegalStateException,
            SecurityException, HeuristicMixedException, HeuristicRollbackException,
            RollbackException, EntityExistsException, javax.transaction.RollbackException {
        ut.begin();
        Car car = new Car( carId, name, productionYear, power, status, price, cartypes);
        em.persist( car);
        if ( cartypes != null) {
            for ( CarType c : cartypes) {
                em.merge( c);
            }
        }
        ut.commit();
        System.out.println( "Crr.add: the car " + car + " was saved.");
    }


    public static List<Car> retrieveAll( EntityManager em) {
        Query query = em.createQuery( "SELECT c FROM Car c", Car.class);
        List<Car> cars = query.getResultList();
        return cars;
    }

    public static Car retrieve(EntityManager em, Integer carId) {
        Car car = em.find( Car.class, carId);
        if ( car != null) {
            System.out.println( "Car.retrieve: loaded car " + car);
        }
        return car;
    }
    /**
     * Update a Book instance
     *
     * @param em
     *          reference to the entity manager
     * @param ut
     *          reference to the user transaction
     * @param carId
     *          the ISBN value of the book to update
     * @param name
     *          the title of the book to update
     * @param productionYear
     *          the year of the book to update
     * @param power
     *          the publisher of the book to update
     * @param status
     *          the authors for the book to update
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public static void update( EntityManager em, UserTransaction ut, Integer carId,
                               String name, Date productionYear, Integer power, StatusEL status, Integer price,
                               Set<CarType> cartypes)
            throws NotSupportedException, SystemException, IllegalStateException,
            SecurityException, HeuristicMixedException, HeuristicRollbackException,
            RollbackException, javax.transaction.RollbackException {
        ut.begin();
        Car car = em.find( Car.class, carId);
        if ( car == null) {
            throw new EntityNotFoundException( "The Car with CarId = " + car
                    + " was not found!");
        }
        Set<CarType> oldCartypes = car.getCarTypes();
        if ( name != null && !name.equals( car.name)) {
            car.setName( name);
        }
        if ( productionYear != null && !productionYear.equals( car.productionYear)) {
            car.setProductionYear( productionYear);
        }
        if (power != null && !power.equals(car.power)) {
            car.setPower(power);
        }
        if ( status != null
                && !status.equals( car.status)) {
            car.setStatus( status);
        }
        if (price != null && !price.equals(car.price)) {
            car.setPrice(price);
        }
        if ( cartypes != null && !cartypes.equals( car.cartypes)) {
            car.setCarTypes( cartypes);
        }
        // merge managed entities in the memory, also update the cached versions
        // and the entity changes are reflected next time when used
        if ( oldCartypes != null) {
            for ( CarType c : oldCartypes) {
                em.merge( c);
            }
        }
        if ( cartypes != null) {
            for ( CarType c : cartypes) {
                em.merge( c);
            }
        }
        ut.commit();
        System.out.println( "Car.update: the car " + car + " was updated.");
    }
    /**
     * Delete a Book instance
     *
     * @param em
     *          reference to the entity manager
     * @param ut
     *          reference to the user transaction
     * @param carId
     *          the carid value of the car to delete
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public static void destroy( EntityManager em, UserTransaction ut, Integer carId)
            throws NotSupportedException, SystemException, IllegalStateException,
            SecurityException, HeuristicMixedException, HeuristicRollbackException,
            RollbackException, javax.transaction.RollbackException {
        ut.begin();
        Car car = em.find( Car.class, carId);
        // find all OfferoftheWeeks which have this car
//        Query query = em.createQuery(
//                "SELECT o FROM OfferOfTheWeek o WHERE o.carId = :carId");
//        query.setParameter( "name", name);
//        List<Book> books = query.getResultList();
//        // clear these books' publisher reference
//        for ( Book b : books) {
//            b.setPublisher( null);
//        }
        car.setCarTypes( null);
        em.remove( car);
        ut.commit();
        System.out.println( "Car.destroy: the car " + car + " was deleted.");
    }
    /**
     * Clear all entries from the <code>books</code> table
     *
     * @param em
     *          reference to the entity manager
     * @param ut
     *          reference to the user transaction
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public static void clearData( EntityManager em, UserTransaction ut)
            throws NotSupportedException, SystemException, IllegalStateException,
            SecurityException, HeuristicMixedException, HeuristicRollbackException,
            RollbackException, javax.transaction.RollbackException {
        ut.begin();
        Query deleteQuery = em.createQuery( "DELETE FROM Car");
        deleteQuery.executeUpdate();
        ut.commit();
    }
//    public static void generateTestData( EntityManager em,
//                                         UserTransaction ut) throws Exception {
//        Car car = null;
//        Car.clearData( em, ut);  // first clear the books table
//        ut.begin();
//        car = new Car(1,"BMW", new Date(2008), 75, StatusEL.FREE, 5000,
//                new HashSet<CarType>());
//        em.persist( car);
//        car = new Car(2,"VW GOLF 2", new Date(2008), 90, StatusEL.FREE, 5000,
//                new HashSet<CarType>());
//        em.persist( car);
//        car = new Car(3,"DeLorian", new Date(2008), 88, StatusEL.FREE, 6000,
//                new HashSet<CarType>());
//        em.persist( car);
//        ut.commit();
//    }


}


