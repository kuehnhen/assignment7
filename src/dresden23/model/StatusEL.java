package dresden23.model;

public enum StatusEL {
    FREE ("free"),
    RESERVED ("reserved"),
    OUTOFORDER ("out of order");

    private final String label;
    private StatusEL( String label) {this.label = label;}
    public String getLabel() {return this.label;}
}
