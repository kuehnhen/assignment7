package dresden23.ctrl;

import dresden23.model.Customer;

import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import java.util.List;

@SessionScoped @ManagedBean( name="customerCtrl")

public class customerController {
    @PersistenceContext( unitName="webapp")
    private EntityManager em;
    @Resource UserTransaction ut;




//    EntityManagerFactory emf =
//            Persistence.createEntityManagerFactory("webapp");
//    EntityManager em = emf.createEntityManager();
//    EntityTransaction et = em.getTransaction();
    /**
     * Read the list of all the customers from the database.
     *
     * @return a list with all the Customer instance found in the database.
     */
    public List<Customer> getCustomers() {
        return Customer.retrieveAll( em);
    }
    /**
     * Update the reference object by setting its property values to match the one
     * existing in the database for the specific instance, identified by the
     * primary key value.
     *
     * @param customer
     *          the reference to the Customer instance to be "loaded" from database.
     */
    public void refreshObject( Customer customer) {
        Customer foundCustomer = Customer.retrieve( em, customer.getCustomerId());
        customer.setFirstName( foundCustomer.getFirstName());
        customer.setSurName( foundCustomer.getSurName());
        customer.setAddress( foundCustomer.getAddress());
    }
    /**
     * Create and persist a new Customer instance.
     *
     * @param customerId
     *          the customerId of the customer to create
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */
    public String create(int customerId, String firstname, String surname, String address) {
        try {
            Customer.create( em, ut, customerId, firstname, surname, address);
            // Clear the form after creating the Customer record
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.getExternalContext().getRequestMap().remove( "customer");
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "create";
    }

    /**
     * Update a Customer instance.
     *
     * @param customerId
     *          the customerId of the customer to update (the customer will be identified in the
     *          database by using this value)
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */

    public String update( int customerId, String firstname, String surname, String address) {
        try {
            Customer.update( em, ut, customerId, firstname, surname, address);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "update";
    }
    /**
     * Delete a Customer entry from database.
     *
     * @param customerId
     *          the customerId of the customer to delete - used to uniquely identify the
     *          customer entry.
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */
    public String delete( int customerId) {    try {
        Customer.delete( em, ut, customerId);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "delete";
    }
    /**
     * Clear/delete all entries from the <code>books</code> table
     *
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public String clearData() {
        try {
            Customer.clearData( em, ut);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "index";
    }

    /**
     * Create test data (rows) in the <code>books</code> table
     *
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public String createTestData() {
        try {
            Customer.generateTestData( em, ut);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "index";
    }




}
