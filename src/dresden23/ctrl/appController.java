package dresden23.ctrl;


import dresden23.model.*;

import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import java.util.Date;
import java.util.HashSet;

@SessionScoped @ManagedBean( name="appCtrl")
public class appController {
    @PersistenceContext( unitName="webapp")
    private EntityManager em;
    @Resource private UserTransaction ut;

    /**
     * Clear/delete all entries from the database
     *
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public String clearData() {
        try {
            Car.clearData( em, ut);
            CarType.clearData( em, ut);
            Customer.clearData( em, ut);
            OfferOfTheWeek.clearData( em, ut);
            PickUpPoint.clearData( em, ut);
            Rental.clearData( em, ut);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "index";
    }

    /**
     * Create test data (rows) in the database
     *
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public String createTestData() {
        try {
//            this.clearData();
//            final Publisher p1, p2;
            final CarType ct1, ct2, ct3;
            final Car c1, c2, c3;
            // clear existing publishers, so no primary key duplicate conflicts appear
//            Publisher.clearData( em, ut);
//            ut.begin();
//            p1 = new Publisher( "Bantam Books", "New York, USA",
//                    new HashSet<Book>());
//            em.persist( p1);
//            p2 = new Publisher( "Basic Books", "New York, USA", new HashSet<Book>());
//            em.persist( p2);
            ct1 = new CarType( 1, "Compact", new HashSet<Car>());
            em.persist( ct1);
            ct2 = new CarType( 2, "Small", new HashSet<Car>());
            em.persist( ct2);
            ct3 = new CarType( 3, "Medium", new HashSet<Car>());
            em.persist( ct3);
            c1 = new Car(1,"BMW", new Date(2008), 75, StatusEL.FREE, 5000,
                    new HashSet<CarType>());
            c1.addCarType( ct1);
            c1.addCarType( ct2);
            em.persist( c1);
            c2 = new Car(2,"VW GOLF 2", new Date(2008), 90, StatusEL.FREE, 5000,
                    new HashSet<CarType>());
            c2.addCarType( ct2);
            em.persist( c2);
            c3 = new Car(3,"DeLorian", new Date(2008), 88, StatusEL.FREE, 6000,
                    new HashSet<CarType>());
            c3.addCarType( ct3);
            em.persist( c3);
//            // update publisher's published books
//            p1.addPublishedBook( b1);
//            p2.addPublishedBook( b2);
            // update author's authored books
            ct1.addCartypedCar( c1);
            ct2.addCartypedCar( c1);
            ct2.addCartypedCar( c2);
            ct3.addCartypedCar( c3);
            ut.commit();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "index";
    }
}