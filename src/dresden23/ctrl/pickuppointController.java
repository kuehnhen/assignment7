package dresden23.ctrl;

import dresden23.model.PickUpPoint;

import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import java.util.List;

@SessionScoped @ManagedBean( name="pickuppointCtrl")

public class pickuppointController {
    @PersistenceContext( unitName="webapp")
    private EntityManager em;
    @Resource UserTransaction ut;




//    EntityManagerFactory emf =
//            Persistence.createEntityManagerFactory("webapp");
//    EntityManager em = emf.createEntityManager();
//    EntityTransaction et = em.getTransaction();
    /**
     * Read the list of all the pickuppoints from the database.
     *
     * @return a list with all the PickUpPoint instance found in the database.
     */
    public List<PickUpPoint> getPickuppoints() {
        return PickUpPoint.retrieveAll( em);
    }
    /**
     * Update the reference object by setting its property values to match the one
     * existing in the database for the specific instance, identified by the
     * primary key value.
     *
     * @param pickuppoint
     *          the reference to the PickUpPoint instance to be "loaded" from database.
     */
    public void refreshObject( PickUpPoint pickuppoint) {
        PickUpPoint foundPickuppoint = PickUpPoint.retrieve( em, pickuppoint.getPointId());
        pickuppoint.setName( foundPickuppoint.getName());
        pickuppoint.setAddress( foundPickuppoint.getAddress());
    }
    /**
     * Create and persist a new PickUpPoint instance.
     *
     * @param pointId
     *          the pointId of the pickuppoint to create
     * @param name
     *          the name of the pickuppoint to create
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */
    public String create(int pointId, String name, String address) {
        try {
            PickUpPoint.create( em, ut, pointId, name, address);
            // Clear the form after creating the PickUpPoint record
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.getExternalContext().getRequestMap().remove( "pickuppoint");
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "create";
    }

    /**
     * Update a PickUpPoint instance.
     *
     * @param pointId
     *          the pointId of the pickuppoint to update (the pickuppoint will be identified in the
     *          database by using this value)
     * @param name
     *          the new value for the name property
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */

    public String update( int pointId, String name, String address) {
        try {
            PickUpPoint.update( em, ut, pointId, name, address);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "update";
    }
    /**
     * Delete a PickUpPoint entry from database.
     *
     * @param pointId
     *          the pointId of the pickuppoint to delete - used to uniquely identify the
     *          pickuppoint entry.
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */
    public String delete( int pointId) {    try {
        PickUpPoint.delete( em, ut, pointId);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "delete";
    }
    /**
     * Clear/delete all entries from the <code>books</code> table
     *
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public String clearData() {
        try {
            PickUpPoint.clearData( em, ut);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "index";
    }

    /**
     * Create test data (rows) in the <code>books</code> table
     *
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public String createTestData() {
        try {
            PickUpPoint.generateTestData( em, ut);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "index";
    }




}
