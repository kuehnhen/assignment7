package dresden23.ctrl;

import dresden23.model.Car;
import dresden23.model.CarType;
import dresden23.model.StatusEL;
import dresden23.model.exception.IntervalConstraintViolation;
import dresden23.model.exception.MandatoryValueConstraintViolation;
import dresden23.model.exception.UniquenessConstraintViolation;
import org.apache.openjpa.persistence.EntityExistsException;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@SessionScoped @ManagedBean( name="carCtrl")

public class carController {
    @PersistenceContext( unitName="webapp")
    private EntityManager em;
    @Resource UserTransaction ut;



    /**
     * Get a reference to the entity manager
     *
     * @return reference to the entitiy manager
     */
    public EntityManager getEntityManager() {
        return this.em;
    }


//    EntityManagerFactory emf =
//            Persistence.createEntityManagerFactory("webapp");
//    EntityManager em = emf.createEntityManager();
//    EntityTransaction et = em.getTransaction();
    /**
     * Read the list of all the cars from the database.
     *
     * @return a list with all the Car instance found in the database.
     */
    public List<Car> getCars() {
        return Car.retrieveAll( em);
    }
    /**
     * Update the reference object by setting its property values to match the one
     * existing in the database for the specific instance, identified by the
     * primary key value.
     *
     * @param car
     *          the reference to the Car instance to be "loaded" from database.
     */
    public void refreshObjects ( Car car) {
        Car foundCar = Car.retrieve( em, car.getCarId());
        car.setName( foundCar.getName());
        car.setProductionYear( foundCar.getProductionYear());
        car.setStatus( foundCar.getStatus());
        car.setPower( foundCar.getPower());
        car.setPrice( foundCar.getPrice());
        car.setCarTypes(foundCar.getCarTypes());
    }
    /**
     * Create and persist a new Car instance.
     *
     * @param carId
     *          the carId of the book to create
     * @param name
     *          the name of the book to create
     * @param productionYear
     *          the productionYear of the book to create
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */
    public String create(Integer carId, String name, Date productionYear, Integer power, StatusEL status,
                         Integer price, Set<CarType> cartypes) {
        try {
            Car.create( em, ut, carId, name, productionYear, power, status, price, cartypes);
            // Enforce clearing the form after creating the Car row.
            // Without this, the form will show the latest completed data
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.getExternalContext().getRequestMap().remove( "car");
        } catch ( EntityExistsException e) {
            try {
                ut.rollback();
            } catch ( Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "create";
    }

    /**
     * Update a Car instance.
     *
     * @param carId
     *          the carId of the car to update (the car will be identified in the
     *          database by using this value)
     * @param name
     *          the new value for the name property
     * @param productionYear
     *          the new value for the productionYear property
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */

    public String update( Integer carId, String name, Date productionYear, Integer power, StatusEL status,
                          Integer price, Set<CarType> cartypes) {
        try {
            Car.update( em, ut, carId, name, productionYear, power, status, price, cartypes);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "update";
    }
    /**
     * Delete a Car entry from database.
     *
     * @param carId
     *          the carId of the car to delete - used to uniquely identify the
     *          car entry.
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */
    public String destroy( Integer carId) {    try {
        Car.destroy( em, ut, carId);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "delete";
    }
    /**
     * Clear/delete all entries from the <code>books</code> table
     *
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public String clearData() {
        try {
            Car.clearData( em, ut);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "index";
    }

    /**
     * Create test data (rows) in the <code>books</code> table
     *
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
//    public String createTestData() {
//        try {
//            Car.generateTestData( em, ut);
//        } catch ( Exception e) {
//            e.printStackTrace();
//        }
//        return "index";
//    }

    /**
     * UI specific check for the carId uniqueness constraint. It uses the
     * <code>Car.checkIsbnAsId</code> method to verify the existence in the
     * database of a book entry for the given carId value.
     *
     * @param context
     *          the faces context - used by the system when the method is
     *          automatically called from JSF facelets.
     * @param component
     *          the UI component reference - used by the system when the method is
     *          automatically called from JSF facelets.
     * @param value
     *          the value to be checked - in this case is the carId to look for in
     *          the database
     * @throws ValidatorException
     */
    public void checkCarIdAsId( FacesContext context, UIComponent component,
                               Object value) throws ValidatorException {
        Integer carId = (Integer) value;
        try {
            Car.checkCarIdAsId( em, carId);
        } catch (UniquenessConstraintViolation | IntervalConstraintViolation e) {
            throw new ValidatorException( new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }

    public void checkName( FacesContext context, UIComponent component,
                                Object value) throws ValidatorException, MandatoryValueConstraintViolation {
        String name = (String) value;
        try {
            Car.checkName( em, name);
        } catch (MandatoryValueConstraintViolation e) {
            throw new ValidatorException( new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }



    public void checkPower( FacesContext context, UIComponent component,
                                Object value) throws ValidatorException {
        Integer power = (Integer) value;
        try {
            Car.checkPower( em, power);
        } catch ( IntervalConstraintViolation e) {
            throw new ValidatorException( new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }
}
