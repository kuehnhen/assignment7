package dresden23.ctrl;

import dresden23.model.Car;
import dresden23.model.CarType;
import dresden23.model.exception.IntervalConstraintViolation;
import dresden23.model.exception.MandatoryValueConstraintViolation;
import dresden23.model.exception.UniquenessConstraintViolation;
import org.apache.openjpa.persistence.EntityExistsException;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import java.util.List;
import java.util.Set;

@SessionScoped @ManagedBean( name="cartypeCtrl")

public class cartypeController {
    @PersistenceContext( unitName="webapp")
    private EntityManager em;
    @Resource UserTransaction ut;

    /**
     * Get a reference to the entity manager
     *
     * @return reference to the entitiy manager
     */
    public EntityManager getEntityManager() {
        return this.em;
    }


//    EntityManagerFactory emf =
//            Persistence.createEntityManagerFactory("webapp");
//    EntityManager em = emf.createEntityManager();
//    EntityTransaction et = em.getTransaction();
    /**
     * Read the list of all the cars from the database.
     *
     * @return a list with all the CarType instance found in the database.
     */
    public List<CarType> getCarTypes() {
        return CarType.retrieveAll( em);
    }
    /**
     * Update the reference object by setting its property values to match the one
     * existing in the database for the specific instance, identified by the
     * primary key value.
     *
     * @param cartype
     *          the reference to the CarType instance to be "loaded" from database.
     */
    public void refreshObjects( CarType cartype) {
        CarType foundCarType = CarType.retrieve( em, cartype.getCartypeId());
        cartype.setCartypeId(foundCarType.getCartypeId());
        cartype.setName( foundCarType.getName());
        cartype.setCartypedCars(foundCarType.getCartypedCars());
    }
    /**
     * Create and persist a new CarType instance.
     *
     * @param cartypeId
     *          the cartypeId of the cartype to create
     * @param name
     *          the name of the cartype to create
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */
    public String create( Integer cartypeId, String name, Set<Car> cartypedCars) {
        try {
            CarType.create( em, ut, cartypeId, name, cartypedCars);
            // Enforce clearing the form after creating the CarType row.
            // Without this, the form will show the latest completed data
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.getExternalContext().getRequestMap().remove( "cartype");
        } catch ( EntityExistsException e) {
            try {
                ut.rollback();
            } catch ( Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "create";
    }
    /**
     * Update a CarType instance.
     *
     * @param cartypeId
     *          the cartypeId of the cartype to update (the cartype will be identified in the
     *          database by using this value)
     * @param name
     *          the new value for the name property
     *          the new value for the productionyear property
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */

    public String update( Integer cartypeId, String name, Set<Car> cartypedCars) {
        try {
            CarType.update( em, ut, cartypeId, name, cartypedCars);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "update";
    }
    /**
     * Delete a CarType entry from database.
     *
     * @param cartypeId
     *          the cartypeId of the cartype to delete - used to uniquely identify the
     *          cartype entry.
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     */
    public String destroy( Integer cartypeId) {    try {
        CarType.destroy( em, ut, cartypeId);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "delete";
    }

    /**
     * UI specific check for the carId uniqueness constraint. It uses the
     * <code>Car.checkIsbnAsId</code> method to verify the existence in the
     * database of a book entry for the given carId value.
     *
     * @param context
     *          the faces context - used by the system when the method is
     *          automatically called from JSF facelets.
     * @param component
     *          the UI component reference - used by the system when the method is
     *          automatically called from JSF facelets.
     * @param value
     *          the value to be checked - in this case is the carId to look for in
     *          the database
     * @throws ValidatorException
     */
    public void checkCarTypeIdAsId( FacesContext context, UIComponent component,
                                Object value) throws ValidatorException {
        Integer cartypeId = (Integer) value;
        try {
            CarType.checkCarTypeIdAsId( em, cartypeId);
        } catch (UniquenessConstraintViolation | IntervalConstraintViolation e) {
            throw new ValidatorException( new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }

    public void checkName( FacesContext context, UIComponent component,
                           Object value) throws ValidatorException, MandatoryValueConstraintViolation {
        String name = (String) value;
        try {
            CarType.checkName( em, name);
        } catch (MandatoryValueConstraintViolation e) {
            throw new ValidatorException( new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }



    /**
     * Clear/delete all entries from the <code>cartypes</code> table
     *
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public String clearData() {
        try {
            CarType.clearData( em, ut);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "index";
    }

    /**
     * Create test data (rows) in the <code>cartypes</code> table
     *
     * @return a string representing the view name to display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
//    public String createTestData() {
//        try {
//            CarType.generateTestData( em, ut);
//        } catch ( Exception e) {
//            e.printStackTrace();
//        }
//        return "index";
//    }




}
