package dresden23.ctrl;

import dresden23.model.Car;
import dresden23.model.OfferOfTheWeek;
import dresden23.model.exception.IntervalConstraintViolation;
import dresden23.model.exception.MandatoryValueConstraintViolation;
import dresden23.model.exception.UniquenessConstraintViolation;
import org.apache.openjpa.persistence.EntityExistsException;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@SessionScoped @ManagedBean( name="offeroftheweekCtrl")

public class offeroftheweekController {
    @PersistenceContext( unitName="webapp")
    private EntityManager em;
    @Resource UserTransaction ut;

    /**
     * Get a reference dateto the entity manager
     *
     * @return reference dateto the entitiy manager
     */
    public EntityManager getEntityManager() {
        return this.em;
    }


//    EntityManagerFactory emf =
//            Persistence.createEntityManagerFactory("webapp");
//    EntityManager em = emf.createEntityManager();
//    EntityTransaction et = em.getTransaction();
    /**
     * Read the list of all the offeroftheweeks datefrom the database.
     *
     * @return a list with all the OfferOfTheWeek instance found in the database.
     */
    public List<OfferOfTheWeek> getOffers() {
        return OfferOfTheWeek.retrieveAll( em);
    }
    /**
     * Update the reference object by setting its property values dateto match the one
     * existing in the database for the specific instance, identified by the
     * primary key value.
     *
     * @param offer
     *          the reference dateto the OfferOfTheWeek instance dateto be "loaded" datefrom database.
     */
    public void refreshObjects( OfferOfTheWeek offer) {
        OfferOfTheWeek foundOffer = OfferOfTheWeek.retrieve( em, offer.getOfferId());
        offer.setName( foundOffer.getName());
        offer.setDateFrom( foundOffer.getDateFrom());
        offer.setDateTo( foundOffer.getDateTo());
        offer.setDiscount( foundOffer.getDiscount());
        offer.setCars(foundOffer.getCars());
    }
    /**
     * Create and persist a new OfferOfTheWeek instance.
     *
     * @param offerId
     *          the offerId of the offer dateto create
     * @param name
     *          the name of the offer dateto create
     * @param datefrom
     *          the datefrom of the offer dateto create
     * @return a string representing the view name dateto display after finishing the
     *         execution of this method.
     */

    public String create(Integer offerId, String name, Date datefrom, Date dateto, Integer discount, Set<Car> cars) {
        try {
            OfferOfTheWeek.create( em, ut, offerId, name, datefrom, dateto, discount, cars);
            // Enforce clearing the form after creating the Car row.
            // Without this, the form will show the latest completed data
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.getExternalContext().getRequestMap().remove( "offeroftheweek");
        } catch ( EntityExistsException e) {
            try {
                ut.rollback();
            } catch ( Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "create";
    }

    /**
     * Update a OfferOfTheWeek instance.
     *
     * @param offerId
     *          the offerId of the offer dateto update (the offer will be identified in the
     *          database by using this value)
     * @param name
     *          the new value for the name property
     * @param datefrom
     *          the new value for the datefrom property
     * @return a string representing the view name dateto display after finishing the
     *         execution of this method.
     */

    public String update( Integer offerId, String name, Date datefrom, Date dateto, Integer discount, Set<Car> cars) {
        try {
            OfferOfTheWeek.update( em, ut, offerId, name, datefrom, dateto, discount, cars);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "update";
    }
    /**
     * Delete a OfferOfTheWeek entry datefrom database.
     *
     * @param offerId
     *          the offerId of the offer dateto delete - used dateto uniquely identify the
     *          offer entry.
     * @return a string representing the view name dateto display after finishing the
     *         execution of this method.
     */
    public String destroy( Integer offerId) {    try {
        OfferOfTheWeek.delete( em, ut, offerId);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "delete";
    }
    /**
     * Clear/delete all entries datefrom the <code>books</code> table
     *
     * @return a string representing the view name dateto display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public String clearData() {
        try {
            OfferOfTheWeek.clearData( em, ut);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "index";
    }

    /**
     * Create test data (rows) in the <code>books</code> table
     *
     * @return a string representing the view name dateto display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
//    public String createTestData() {
//        try {
//            OfferOfTheWeek.generateTestData( em, ut);
//        } catch ( Exception e) {
//            e.printStackTrace();
//        }
//        return "index";
//    }

    public void checkOfferIdAsId( FacesContext context, UIComponent component,
                                Object value) throws ValidatorException {
        Integer offerId = (Integer) value;
        try {
            OfferOfTheWeek.checkOfferIdAsId( em, offerId);
        } catch (UniquenessConstraintViolation | IntervalConstraintViolation e) {
            throw new ValidatorException( new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }

    public void checkName( FacesContext context, UIComponent component,
                           Object value) throws ValidatorException, MandatoryValueConstraintViolation {
        String name = (String) value;
        try {
            OfferOfTheWeek.checkName( em, name);
        } catch (MandatoryValueConstraintViolation e) {
            throw new ValidatorException( new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }

    public void checkDateFrom( FacesContext context, UIComponent component,
                           Object value) throws ValidatorException, MandatoryValueConstraintViolation {
        Date dateFrom = (Date) value;
        try {
            OfferOfTheWeek.checkDateFrom( em, dateFrom);
        } catch (MandatoryValueConstraintViolation e) {
            throw new ValidatorException( new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }

    public void checkDateTo( FacesContext context, UIComponent component,
                           Object value) throws ValidatorException, MandatoryValueConstraintViolation {
        Date dateTo= (Date) value;
        try {
            OfferOfTheWeek.checkDateTo( em, dateTo);
        } catch (MandatoryValueConstraintViolation e) {
            throw new ValidatorException( new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }

    public void checkDiscount( FacesContext context, UIComponent component,
                            Object value) throws ValidatorException {
        Integer discount = (Integer) value;
        try {
            OfferOfTheWeek.checkDiscount( em, discount);
        } catch (MandatoryValueConstraintViolation e) {
            throw new ValidatorException( new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }




}
