package dresden23.ctrl;

import dresden23.model.Car;
import dresden23.model.Customer;
import dresden23.model.PickUpPoint;
import dresden23.model.Rental;
import org.apache.openjpa.persistence.EntityExistsException;

import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import java.util.Date;
import java.util.List;

@SessionScoped @ManagedBean( name="rentalCtrl")

public class rentalController {
    @PersistenceContext( unitName="webapp")
    private EntityManager em;
    @Resource UserTransaction ut;




//    EntityManagerFactory emf =
//            Persistence.createEntityManagerFactory("webapp");
//    EntityManager em = emf.createEntityManager();
//    EntityTransaction et = em.getTransaction();
    /**
     * Read the list of all the rentals rentalfrom the database.
     *
     * @return a list with all the Rental instance found in the database.
     */
    public List<Rental> getRentals() {
        return Rental.retrieveAll( em);
    }
    /**
     * Update the reference object by setting its property values rentalto match the one
     * existing in the database for the specific instance, identified by the
     * primary key value.
     *
     * @param rental
     *          the reference rentalto the Rental instance rentalto be "loaded" rentalfrom database.
     */
    public void refreshObjects( Rental rental) {
        Rental foundRental = Rental.retrieve( em, rental.getRentalId());
        rental.setRentaldate( foundRental.getRentaldate());
        rental.setRentalfrom( foundRental.getRentalfrom());
        rental.setRentalto( foundRental.getRentalto());
        rental.setDiscount( foundRental.getDiscount());
        rental.setDeliveryToThePoint( foundRental.getDeliveryToThePoint());
        rental.setDeliveryaddress( foundRental.getDeliveryaddress());
        rental.setTotal( foundRental.getTotal());
        rental.setCar(foundRental.getCar());
        rental.setPickuppoint(foundRental.getPickuppoint());
        rental.setCustomer(foundRental.getCustomer());
    }
    /**
     * Create and persist a new Rental instance.
     *
     * @param rentalId
     *          the rentalId of the rental rentalto create
     * @return a string representing the view name rentalto display after finishing the
     *         execution of this method.
     */

    public String create(Integer rentalId, Date rentaldate, Date rentalfrom, Date rentalto, Integer discount,
                         boolean deliverytothepoint, String deliveryaddress, Integer total, Car car,
                         PickUpPoint pickUpPoint, Customer customer) {
        try {
            Rental.create( em, ut, rentalId, rentaldate, rentalfrom, rentalto, discount, deliverytothepoint,
                    deliveryaddress, total, car, pickUpPoint, customer);
            // Enforce clearing the form after creating the Car row.
            // Without this, the form will show the latest completed data
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.getExternalContext().getRequestMap().remove( "rental");
        } catch ( EntityExistsException e) {
            try {
                ut.rollback();
            } catch ( Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "create";
    }

    /**
     * Update a Rental instance.
     *
     * @param rentalId
     *          the rentalId of the rental rentalto update (the rental will be identified in the
     *          database by using this value)
     * @return a string representing the view name rentalto display after finishing the
     *         execution of this method.
     */

    public String update(Integer rentalId, Date rentaldate, Date rentalfrom, Date rentalto, Integer discount,
                         boolean deliverytothepoint, String deliveryaddress, Integer total, Car car,
                         PickUpPoint pickUpPoint, Customer customer) {
        try {
            Rental.update( em, ut, rentalId, rentaldate, rentalfrom, rentalto, discount, deliverytothepoint,
                    deliveryaddress, total, car, pickUpPoint, customer);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "update";
    }
    /**
     * Delete a Rental entry rentalfrom database.
     *
     * @param rentalId
     *          the rentalId of the rental rentalto delete - used rentalto uniquely identify the
     *          rental entry.
     * @return a string representing the view name rentalto display after finishing the
     *         execution of this method.
     */
    public String delete( Integer rentalId) {    try {
        Rental.delete( em, ut, rentalId);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "delete";
    }
    /**
     * Clear/delete all entries rentalfrom the <code>books</code> table
     *
     * @return a string representing the view name rentalto display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
    public String clearData() {
        try {
            Rental.clearData( em, ut);
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return "index";
    }

    /**
     * Create test data (rows) in the <code>books</code> table
     *
     * @return a string representing the view name rentalto display after finishing the
     *         execution of this method.
     *
     * @throws NotSupportedException
     * @throws SystemException
     * @throws IllegalStateException
     * @throws SecurityException
     * @throws HeuristicMixedException
     * @throws HeuristicRollbackException
     * @throws RollbackException
     */
//    public String createTestData() {
//        try {
//            Rental.generateTestData( em, ut);
//        } catch ( Exception e) {
//            e.printStackTrace();
//        }
//        return "index";
//    }




}
